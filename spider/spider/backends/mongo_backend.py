from __future__ import absolute_import

from frontera.contrib.backends import CommonBackend
from spider.backends.mongo_components import Metadata as MongoMetadata, Queue, States
from frontera.exceptions import NotConfigured
from pymongo import MongoClient


class MongodbCustomizeBackend(CommonBackend):
    def __init__(self, manager):
        self.manager = manager
        settings = manager.settings
        drop_all_tables = settings.get('SQLALCHEMYBACKEND_DROP_ALL_TABLES')
        clear_content = settings.get('SQLALCHEMYBACKEND_CLEAR_CONTENT')

        # mongo_hostname = settings.get('MONGO_HOST')
        # mongo_port = settings.get('MONGO_PORT')
        mongo_uri = settings.get('MONGODBBACKEND_ENGINE')
        mongo_db = settings.get('MONGO_DATABASE')
        if mongo_uri is None or mongo_db is None is None:
            raise NotConfigured

        self.client = MongoClient(mongo_uri)
        self.db = self.client[mongo_db]
        if clear_content:
            for collec in  self.db.collection_names():
                self.db[collec].remove({})
        if drop_all_tables:
            self.db['metadata'].drop()
            self.db['queue'].drop()
            self.db['state'].drop()

        self.db['metadata'].ensure_index("fingerprint", unique=True, drop_dups=True)
        self.db['queue'].ensure_index("fingerprint", drop_dups=True)
        self.db['queue'].ensure_index("score")
        self.db['queue'].ensure_index("partition_id")
        self.db['queue'].ensure_index("create_at")
        self.db['state'].ensure_index("fingerprint", unique=True, drop_dups=True)
        self._metadata = MongoMetadata(self.db['metadata'], settings.get('SQLALCHEMYBACKEND_CACHE_SIZE'))
        self._states = States(self.db['state'], settings.get('STATE_CACHE_SIZE_LIMIT'))
        self._queue = self._create_queue(settings)


    def frontier_stop(self):
        super(MongodbCustomizeBackend, self).frontier_stop()
        # self.engine.dispose()

    def _create_queue(self, settings):
        return Queue(self.db['queue'], settings.get('SPIDER_FEED_PARTITIONS'))

    @property
    def queue(self):
        return self._queue

    @property
    def metadata(self):
        return self._metadata

    @property
    def states(self):
        return self._states


class FIFOBackend(MongodbCustomizeBackend):
    component_name = 'SQLAlchemy FIFO Backend'

    def _create_queue(self, settings):
        return Queue(self.db['queue'], settings.get('SPIDER_FEED_PARTITIONS'), ordering='created')


class LIFOBackend(MongodbCustomizeBackend):
    component_name = 'SQLAlchemy LIFO Backend'

    def _create_queue(self, settings):
        return Queue(self.db['queue'], settings.get('SPIDER_FEED_PARTITIONS'), ordering='created_desc')

class UpdateBackend(MongodbCustomizeBackend):
    component_name = 'SQLAlchemy Update Backend'

    def _create_queue(self, settings):
        return Queue(self.db['queue'], settings.get('SPIDER_FEED_PARTITIONS'), ordering='updated')

class DFSBackend(MongodbCustomizeBackend):
    component_name = 'SQLAlchemy DFS Backend'

    def _create_queue(self, settings):
        return Queue(self.db['queue'], settings.get('SPIDER_FEED_PARTITIONS'))

    def _get_score(self, obj):
        return -obj.meta['depth']


class BFSBackend(MongodbCustomizeBackend):
    component_name = 'SQLAlchemy BFS Backend'

    def _create_queue(self, settings):
        return Queue(self.db['queue'], settings.get('SPIDER_FEED_PARTITIONS'))

    def _get_score(self, obj):
        return obj.meta['depth']


BASE = CommonBackend
LIFO = LIFOBackend
FIFO = FIFOBackend
DFS = DFSBackend
BFS = BFSBackend
UPDATE = UpdateBackend


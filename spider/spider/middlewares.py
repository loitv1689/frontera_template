# Copyright (C) 2013 by Aivars Kalvans <aivars.kalvans@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import re
from random import randint
import base64
import pymongo
from scrapy import log
from scrapy.exceptions import IgnoreRequest
from fake_useragent import UserAgent
from scrapy.http import Request
import cfscrape
import random
from scrapy import signals
from scrapy.downloadermiddlewares.useragent import UserAgentMiddleware

class RandomProxy(object):
    collection_name = 'webpage'

    def __init__(self, settings, mongo_uri, mongo_db):
        self.is_proxy = True
        self.settings = settings
        self.proxies = {}
        self.mongo_uri = mongo_uri
        self.mongo_db = mongo_db
        self.client = pymongo.MongoClient(self.mongo_uri)
        self.db = self.client[self.mongo_db]

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            crawler.settings,
            mongo_uri=crawler.settings.get('MONGO_URI'),
            mongo_db=crawler.settings.get('MONGO_DATABASE')
        )

    def process_request(self, request, spider):
        # Don't overwrite with a random one (server-side state for IP)
        if self.settings.get('GLOBAL_PROXY_ENABLE', False):
            request.meta['X-ProxyMesh-Country'] = random.choice(['US','SG'])
            request.meta['proxy'] = 'http://open.proxymesh.com:31280'
            return
        if not self.is_proxy:
            return
        if not len(self.proxies) > 0:
            self.load_proxy()
            if not len(self.proxies) > 0:
                self.is_proxy = False
                return

        if 'proxy' in request.meta and request.meta['proxy'] in self.proxies:
            return

        proxy_address = random.choice(self.proxies.keys())
        proxy_user_pass = self.proxies[proxy_address]

        request.meta['proxy'] = proxy_address.strip()
        request.meta['X-ProxyMesh-Country'] = 'SG'#random.choice(['US','SG'])
        if proxy_user_pass:
            request.headers['Proxy-Authorization'] = 'Basic ' + base64.encodestring(proxy_user_pass).replace('\n', '')
        return
    def process_exception(self, request, exception, spider):
        try:
            if request.meta.has_key('proxy'):
                proxy = request.meta['proxy']
                log.msg('Removing failed proxy <%s>, %d proxies left' % (
                        proxy, len(self.proxies)))
                if self.proxies.has_key(proxy):
                    del self.proxies[proxy]
                    self.db.proxy.update_one(
                        {"_id": proxy},
                        { '$inc': {'die_num': 1}}
                    )
        except ValueError:
            pass
        except Exception, ex:
            print ex

    def load_proxy(self):
        self.client = pymongo.MongoClient(self.mongo_uri)
        min = self.db.proxy.find_one(sort=[("die_num", 1)])["die_num"]
        proxies =  self.db.proxy.find({'die_num':{'$lte':min}})
        for line in proxies:
            parts = re.match('(\w+://)(\w+:\w+@)?(.+)', line['_id'])
            if not parts:
                continue
            # Cut trailing @
            if parts.group(2):
                user_pass = parts.group(2)[:-1]
            else:
                user_pass = ''
            self.proxies[parts.group(1) + parts.group(3)] = user_pass

class FilterLinks(object):

    def process_request(self, request, spider):
        return
        # if request.url == 'http://mangafox.me/':
        #     return
        # elif  re.search('mangafox\.me/manga/[^/]*/$', request.url) is not None:
        #     return
        # elif re.search('mangafox\.me/manga/.*/[0-9]+\.html$', request.url) is not None:
        #     return
        # elif re.search('m.mangafox\.me/roll_manga/.*/[0-9]+\.html$', request.url) is not None:
        #     return
        # elif  re.search('mangafox\.me.*/releases/', request.url) is not None:
        #     return
        # else:
        #     raise IgnoreRequest()


class FilterLinksReCrawl(object):

    def process_request(self, request, spider):
        for pattern in spider.lists_except:
            if re.search(pattern, request.url) is not None:
                # spider.lists_recrawl.append(request.url)
                raise IgnoreRequest()

class RandomUserAgentMiddleware(UserAgentMiddleware):

    def __init__(self, settings, user_agent='Scrapy'):
        super(RandomUserAgentMiddleware, self).__init__()
        self.user_agent = user_agent
        self.list_indentifies = {}
        self.settings = settings
        user_agent_list_file = settings.get('USER_AGENT_LIST')
        if not user_agent_list_file:
            # If USER_AGENT_LIST_FILE settings is not set,
            # Use the default USER_AGENT or whatever was
            # passed to the middleware.
            ua = settings.get('USER_AGENT', user_agent)
            self.user_agent_list = [ua]
        else:
            with open(user_agent_list_file, 'r') as f:
                self.user_agent_list = [line.strip() for line in f.readlines()]

    @classmethod
    def from_crawler(cls, crawler):
        obj = cls(crawler.settings)
        crawler.signals.connect(obj.spider_opened,
                                signal=signals.spider_opened)
        return obj

    def process_request(self, request, spider):
        if self.settings.get('CLOUDFARE_ENABLE', False):
            key = randint(0,1)
            if self.list_indentifies.has_key(key):
                token, agent = self.list_indentifies[key]
            else:
                user_agent = random.choice(self.user_agent_list)
                seed_url = "http://kissmanga.com/"
                token, agent = cfscrape.get_tokens(seed_url, user_agent)
                self.list_indentifies[key] = (token, agent)
            request.headers.setdefault('User-Agent', agent)
            request.cookies = token
        else:
            user_agent = random.choice(self.user_agent_list)
            if user_agent:
                request.headers.setdefault('User-Agent', user_agent)



BOT_NAME = 'spider'

SPIDER_MODULES = ['spider.spiders']
NEWSPIDER_MODULE = 'spider.spiders'


MONGO_URI = 'localhost:27017'
MONGO_DATABASE = 'frontera_crawler'

NOTIFI_HOST='http://truyentranh8.dev:8888/'
NOTIFI_PORT=8888
DOMAIN_URL='http://manga23.com/'
TOKEN='DKDFJ2323423ASD'

# LOG_FILE = 'log.txt'
LOG_ENABLED = True
LOG_ENCODING = 'utf-8'
LOG_LEVEL = 'DEBUG'


# Disable cookies (enabled by default)
COOKIES_ENABLED= True
USER_AGENT_LIST = 'useragents.txt'

ITEM_PIPELINES = {
   'spider.pipelines.MongoPipeline': 300,
}


DOWNLOAD_TIMEOUT=10
RETRY_ENABLED = True
RETRY_TIMES =10
CLOSESPIDER_TIMEOUT=1800 #second
# Retry on most error codes since proxies fail for different reasons
RETRY_HTTP_CODES = [500, 503, 504, 400, 403, 404, 408]
DEPTH_PRIORITY = 100
SPIDER_MIDDLEWARES = {}
DOWNLOADER_MIDDLEWARES = {}
SPIDER_MIDDLEWARES.update({
    'frontera.contrib.scrapy.middlewares.seeds.file.FileSeedLoader': 998
})
SPIDER_MIDDLEWARES.update({
    'frontera.contrib.scrapy.middlewares.schedulers.SchedulerSpiderMiddleware': 999,
})

DOWNLOADER_MIDDLEWARES.update({
    'spider.middlewares.FilterLinks':89,
    'scrapy.downloadermiddlewares.retry.RetryMiddleware': 90,
    # 'spider.middlewares.RandomProxy':100,
    # 'scrapy.downloadermiddlewares.httpproxy.HttpProxyMiddleware': 110,
    'scrapy.downloadermiddlewares.useragent.UserAgentMiddleware': None,
    'spider.middlewares.RandomUserAgentMiddleware': 400,
    'scrapy.spidermiddlewares.offsite.OffsiteMiddleware' : 624,
    'frontera.contrib.scrapy.middlewares.schedulers.SchedulerDownloaderMiddleware': 999,
})

SCHEDULER = 'frontera.contrib.scrapy.schedulers.frontier.FronteraScheduler'
FRONTERA_SETTINGS = 'spider.frontera_settings'
SEEDS_SOURCE = 'seed'

PROXY_LIST = 'proxylist.txt'

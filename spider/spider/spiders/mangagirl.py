# -*- coding: utf-8 -*-
import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.http import Request


class MangagirlSpider(scrapy.Spider):
    name = "mangagirl"
    allowed_domains = ["www.thetoptens.com"]
    start_urls = ['http://www.thetoptens.com/']
    link_extractor = LinkExtractor(
        allow=([]),
        deny=([]),
        restrict_xpaths=()
    )
    def parse(self, response):
        for link in self.link_extractor.extract_links(response):
            request = Request(url=link.url)
            request.meta.update(link_text=link.text)
            yield request



